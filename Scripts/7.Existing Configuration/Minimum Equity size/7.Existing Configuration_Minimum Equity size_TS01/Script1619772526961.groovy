import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TEST BLOCKS/0.Launch_Criteria Hub Test Lender'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/1.About the Loan/Type/LOAN_Type is PURCHASE'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/1.About the Loan/LTV/Mixed/LOAN_LTV is 60 (MIX)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/1.About the Loan/Term/LOAN_Term is 18yrs'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/1.About the Loan/Number of Applicants/LOAN_Applicants is 1'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/2.About the Property/Property Tenure/PROPERTY_Tenure is FREEHOLD'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/2.About the Property/Location/PROPERTY_Location is ENGLAND'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/0.OPEN ACCORDION (APP1)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/1.Date of Birth/APP1_Applicant is 30'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/2.Number of Dependants/APP1_Dependants is 0'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/3.Planned Retirement Age/APP1_Planned Retirement is 70'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/4.Incomes/Employed/0.OPEN ACCORDION (APP1) (EMP)'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/4.About the Applicants/Applicant 1/4.Incomes/Employed/Length of time Employed in this job (Date)/APP1_EMP_Length of time is 01-01-2020'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Criteria Hub Test Lender/4.About the Applicants/Applicant 1/4.Incomes/Employed/Basic Salary'), 
    '50000')

WebUI.callTestCase(findTestCase('TEST BLOCKS/6. Click_CALCULATE'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('TEST BLOCKS/7.Validation/VALIDATION_Deposit Equity needs to be at least 150,000'), [:], 
    FailureHandling.STOP_ON_FAILURE)

