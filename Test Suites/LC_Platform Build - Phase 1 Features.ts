<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LC_Platform Build - Phase 1 Features</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2a8ae647-84d3-4b77-bf0a-5da2bc831ad7</testSuiteGuid>
   <testCaseLink>
      <guid>08a7e340-919e-477f-9585-af078fa0402b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.Existing Configuration/MinMax Loan size/7.Existing Configuration_MinMax Loan size_TS01</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LenderCalcs/Phase 1 - User Stories</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Type</value>
         <variableId>6a043bbb-d9fa-4b8f-a828-6347a3ee5d68</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Purchase Price</value>
         <variableId>0f3cc9ad-2c2e-438c-ada6-5e935ca69039</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Method</value>
         <variableId>913f66bb-289e-4116-bedd-9383fdbe0e4f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Capital and Interest</value>
         <variableId>eff13f57-70b9-4e89-925c-9340045fc33b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Interest Only</value>
         <variableId>4d3370d0-6c90-4d71-892c-a6ae9f873af7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Term</value>
         <variableId>a652f802-8707-44c2-9abe-4fa1a565c2fb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Number of Applicants</value>
         <variableId>30ad63f0-5df6-4cb6-bd31-6cdc1a24f1b3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Will consider fixed rate of 5years or more</value>
         <variableId>501b8758-b2a8-429e-a66e-09ef58a73891</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>New Build</value>
         <variableId>8fd0c4eb-5f20-46b2-bff9-0588d7048b57</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df0c3d0d-0dd0-4c46-b8d9-b2470c0bc42e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>EXPECTED_RESULT</value>
         <variableId>eb50b2b2-f8af-42f2-a5cb-3ecf489ed642</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
